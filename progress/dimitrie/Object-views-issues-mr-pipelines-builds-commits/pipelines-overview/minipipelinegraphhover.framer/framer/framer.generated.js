// This is autogenerated by Framer


if (!window.Framer && window._bridge) {window._bridge('runtime.error', {message:'[framer.js] Framer library missing or corrupt. Select File → Update Framer Library.'})}
window.__imported__ = window.__imported__ || {};
window.__imported__["minipipelinegraphhover@1x/layers.json.js"] = [
	{
		"objectId": "1735F3C9-68F2-4170-A462-46A8CB86AA18",
		"kind": "group",
		"name": "Group_2",
		"originalName": "Group 2",
		"maskFrame": null,
		"layerFrame": {
			"x": 30,
			"y": 168,
			"width": 162,
			"height": 106
		},
		"visible": true,
		"metadata": {
			"opacity": 1
		},
		"children": [
			{
				"objectId": "4CAE46C0-29A3-4A92-94AC-B1ADC3100431",
				"kind": "group",
				"name": "Group_3_Copy_2",
				"originalName": "Group 3 Copy 2",
				"maskFrame": null,
				"layerFrame": {
					"x": 30,
					"y": 238,
					"width": 162,
					"height": 36
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"children": [
					{
						"objectId": "477F1C1A-017C-4BA3-839B-CD1F9568ABCB",
						"kind": "group",
						"name": "icon_running",
						"originalName": "icon-running",
						"maskFrame": null,
						"layerFrame": {
							"x": 270,
							"y": 238,
							"width": 36,
							"height": 36
						},
						"visible": false,
						"metadata": {
							"opacity": 1
						},
						"children": [
							{
								"objectId": "A3ED56FE-6FAD-4C3E-8E03-8C2AAA86E14D",
								"kind": "group",
								"name": "Group",
								"originalName": "Group",
								"maskFrame": null,
								"layerFrame": {
									"x": 270,
									"y": 238,
									"width": 36,
									"height": 36
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-Group-qtnfrdu2.png",
									"frame": {
										"x": 270,
										"y": 238,
										"width": 36,
										"height": 36
									}
								},
								"children": []
							}
						]
					},
					{
						"objectId": "E079E84A-4B0D-4CA2-84FD-AAA1F6E624D0",
						"kind": "group",
						"name": "icon_pending",
						"originalName": "icon-pending",
						"maskFrame": null,
						"layerFrame": {
							"x": 282,
							"y": 238,
							"width": 36,
							"height": 36
						},
						"visible": false,
						"metadata": {
							"opacity": 1
						},
						"children": [
							{
								"objectId": "6BD2E58A-6921-4137-9E2E-45E1597319F1",
								"kind": "group",
								"name": "Group1",
								"originalName": "Group",
								"maskFrame": null,
								"layerFrame": {
									"x": 282,
									"y": 238,
									"width": 36,
									"height": 36
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-Group-nkjemku1.png",
									"frame": {
										"x": 282,
										"y": 238,
										"width": 36,
										"height": 36
									}
								},
								"children": []
							}
						]
					},
					{
						"objectId": "58B59857-63F8-4068-A28E-0629925359BB",
						"kind": "group",
						"name": "icon_passed_copy_2",
						"originalName": "icon-passed copy 2",
						"maskFrame": null,
						"layerFrame": {
							"x": 78,
							"y": 238,
							"width": 36,
							"height": 36
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"children": [
							{
								"objectId": "4F5DE3A7-94A3-4EFF-89CB-1E8E8A27231E",
								"kind": "group",
								"name": "Group2",
								"originalName": "Group",
								"maskFrame": null,
								"layerFrame": {
									"x": 78,
									"y": 238,
									"width": 36,
									"height": 36
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-Group-ney1reuz.png",
									"frame": {
										"x": 78,
										"y": 238,
										"width": 36,
										"height": 36
									}
								},
								"children": []
							}
						]
					},
					{
						"objectId": "6FD97116-748D-4458-86FB-82A12E0ABCC5",
						"kind": "group",
						"name": "icon_passed",
						"originalName": "icon-passed",
						"maskFrame": null,
						"layerFrame": {
							"x": 30,
							"y": 238,
							"width": 36,
							"height": 36
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"children": [
							{
								"objectId": "9FBE0F89-0A24-4854-8E67-727CD80A4835",
								"kind": "group",
								"name": "Group3",
								"originalName": "Group",
								"maskFrame": null,
								"layerFrame": {
									"x": 30,
									"y": 238,
									"width": 36,
									"height": 36
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-Group-ouzcrtbg.png",
									"frame": {
										"x": 30,
										"y": 238,
										"width": 36,
										"height": 36
									}
								},
								"children": []
							}
						]
					},
					{
						"objectId": "369A7E6B-4E26-483D-9C43-193DD069DDD8",
						"kind": "group",
						"name": "icon_failed",
						"originalName": "icon-failed",
						"maskFrame": null,
						"layerFrame": {
							"x": 126,
							"y": 238,
							"width": 36,
							"height": 36
						},
						"visible": false,
						"metadata": {
							"opacity": 1
						},
						"children": [
							{
								"objectId": "48FB893D-03E8-43B2-A750-4DE80811B16A",
								"kind": "group",
								"name": "Group4",
								"originalName": "Group",
								"maskFrame": null,
								"layerFrame": {
									"x": 126,
									"y": 238,
									"width": 36,
									"height": 36
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-Group-ndhgqjg5.png",
									"frame": {
										"x": 126,
										"y": 238,
										"width": 36,
										"height": 36
									}
								},
								"children": []
							}
						]
					},
					{
						"objectId": "C12D8CB6-46B5-41DF-AA4D-9A2B28FCDB6C",
						"kind": "group",
						"name": "iconfull",
						"originalName": "iconfull",
						"maskFrame": null,
						"layerFrame": {
							"x": 126,
							"y": 238,
							"width": 66,
							"height": 36
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"children": [
							{
								"objectId": "D542EB5D-B222-4831-ACA1-5A9DA570CE64",
								"kind": "group",
								"name": "Icon",
								"originalName": "Icon",
								"maskFrame": null,
								"layerFrame": {
									"x": 126,
									"y": 238,
									"width": 36,
									"height": 36
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-Icon-rdu0mkvc.png",
									"frame": {
										"x": 126,
										"y": 238,
										"width": 36,
										"height": 36
									}
								},
								"children": []
							},
							{
								"objectId": "6E09A95E-7D19-46C3-9277-40AFBBAE0DBD",
								"kind": "text",
								"name": "num",
								"originalName": "num",
								"maskFrame": null,
								"layerFrame": {
									"x": 162,
									"y": 247,
									"width": 22,
									"height": 17
								},
								"visible": true,
								"metadata": {
									"opacity": 1,
									"string": "11",
									"css": [
										"/* 11: */",
										"font-family: SourceSansPro-Regular;",
										"font-size: 26px;",
										"color: #FFFFFF;"
									]
								},
								"image": {
									"path": "images/Layer-num-nkuwoue5.png",
									"frame": {
										"x": 162,
										"y": 247,
										"width": 22,
										"height": 17
									}
								},
								"children": []
							},
							{
								"objectId": "C7A980B1-B92C-467E-8D14-E0E64DEA3EFC",
								"kind": "group",
								"name": "bg",
								"originalName": "bg",
								"maskFrame": null,
								"layerFrame": {
									"x": 126,
									"y": 238,
									"width": 66,
									"height": 36
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-bg-qzdbotgw.png",
									"frame": {
										"x": 126,
										"y": 238,
										"width": 66,
										"height": 36
									}
								},
								"children": []
							}
						]
					},
					{
						"objectId": "4FB3BFF0-47C5-42D6-8A43-20D80AD63BCA",
						"kind": "group",
						"name": "Group_4",
						"originalName": "Group 4",
						"maskFrame": null,
						"layerFrame": {
							"x": 64,
							"y": 254,
							"width": 64,
							"height": 5
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Group_4-nezcm0jg.png",
							"frame": {
								"x": 64,
								"y": 254,
								"width": 64,
								"height": 5
							}
						},
						"children": []
					},
					{
						"objectId": "03BC0ACA-F515-41DC-8BBA-EB02669F599F",
						"kind": "group",
						"name": "icon_created",
						"originalName": "icon-created",
						"maskFrame": null,
						"layerFrame": {
							"x": 78,
							"y": 238,
							"width": 36,
							"height": 36
						},
						"visible": false,
						"metadata": {
							"opacity": 1
						},
						"children": [
							{
								"objectId": "FBADA4F3-5C0B-4AD9-AAD9-912C894A7ABF",
								"kind": "group",
								"name": "Group5",
								"originalName": "Group",
								"maskFrame": null,
								"layerFrame": {
									"x": 78,
									"y": 238,
									"width": 36,
									"height": 36
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-Group-rkjbree0.png",
									"frame": {
										"x": 78,
										"y": 238,
										"width": 36,
										"height": 36
									}
								},
								"children": []
							}
						]
					}
				]
			},
			{
				"objectId": "4B1AC101-504D-4D87-90DF-BA441CF05955",
				"kind": "group",
				"name": "Group_3_Copy",
				"originalName": "Group 3 Copy",
				"maskFrame": null,
				"layerFrame": {
					"x": 30,
					"y": 168,
					"width": 148,
					"height": 36
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"children": [
					{
						"objectId": "6BE43193-505F-4619-B219-B166AA57A7AA",
						"kind": "group",
						"name": "icon_running1",
						"originalName": "icon-running",
						"maskFrame": null,
						"layerFrame": {
							"x": 270,
							"y": 168,
							"width": 36,
							"height": 36
						},
						"visible": false,
						"metadata": {
							"opacity": 1
						},
						"children": [
							{
								"objectId": "797B2902-A5AD-41EC-9E0A-9E81AD133405",
								"kind": "group",
								"name": "Group6",
								"originalName": "Group",
								"maskFrame": null,
								"layerFrame": {
									"x": 270,
									"y": 168,
									"width": 36,
									"height": 36
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-Group-nzk3qji5.png",
									"frame": {
										"x": 270,
										"y": 168,
										"width": 36,
										"height": 36
									}
								},
								"children": []
							}
						]
					},
					{
						"objectId": "76031B0E-D400-4860-83C2-9C437395BD84",
						"kind": "group",
						"name": "icon_pending1",
						"originalName": "icon-pending",
						"maskFrame": null,
						"layerFrame": {
							"x": 282,
							"y": 168,
							"width": 36,
							"height": 36
						},
						"visible": false,
						"metadata": {
							"opacity": 1
						},
						"children": [
							{
								"objectId": "4B9977D9-828C-4E16-96FA-24A024283280",
								"kind": "group",
								"name": "Group7",
								"originalName": "Group",
								"maskFrame": null,
								"layerFrame": {
									"x": 282,
									"y": 168,
									"width": 36,
									"height": 36
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-Group-nei5otc3.png",
									"frame": {
										"x": 282,
										"y": 168,
										"width": 36,
										"height": 36
									}
								},
								"children": []
							}
						]
					},
					{
						"objectId": "6AEDAB7D-2358-4D59-99D9-A159CD93870F",
						"kind": "group",
						"name": "icon_passed_copy_21",
						"originalName": "icon-passed copy 2",
						"maskFrame": null,
						"layerFrame": {
							"x": 78,
							"y": 168,
							"width": 36,
							"height": 36
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"children": [
							{
								"objectId": "ABE46C41-ED76-49A1-B62D-410B677A57D9",
								"kind": "group",
								"name": "Group8",
								"originalName": "Group",
								"maskFrame": null,
								"layerFrame": {
									"x": 78,
									"y": 168,
									"width": 36,
									"height": 36
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-Group-qujfndzd.png",
									"frame": {
										"x": 78,
										"y": 168,
										"width": 36,
										"height": 36
									}
								},
								"children": []
							}
						]
					},
					{
						"objectId": "DEE9C4C5-8849-4F61-842B-AB0B9DAAA848",
						"kind": "group",
						"name": "icon_passed1",
						"originalName": "icon-passed",
						"maskFrame": null,
						"layerFrame": {
							"x": 30,
							"y": 168,
							"width": 36,
							"height": 36
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"children": [
							{
								"objectId": "A682AD23-9289-4F3C-8B99-069019E29604",
								"kind": "group",
								"name": "Group9",
								"originalName": "Group",
								"maskFrame": null,
								"layerFrame": {
									"x": 30,
									"y": 168,
									"width": 36,
									"height": 36
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-Group-qty4mkfe.png",
									"frame": {
										"x": 30,
										"y": 168,
										"width": 36,
										"height": 36
									}
								},
								"children": []
							}
						]
					},
					{
						"objectId": "EAC779EF-15ED-4820-91DF-FFE36352C356",
						"kind": "group",
						"name": "icon_failed1",
						"originalName": "icon-failed",
						"maskFrame": null,
						"layerFrame": {
							"x": 126,
							"y": 168,
							"width": 36,
							"height": 36
						},
						"visible": false,
						"metadata": {
							"opacity": 1
						},
						"children": [
							{
								"objectId": "6B522F1F-02B2-475C-826D-6A7F523E32F2",
								"kind": "group",
								"name": "Group10",
								"originalName": "Group",
								"maskFrame": null,
								"layerFrame": {
									"x": 126,
									"y": 168,
									"width": 36,
									"height": 36
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-Group-nki1mjjg.png",
									"frame": {
										"x": 126,
										"y": 168,
										"width": 36,
										"height": 36
									}
								},
								"children": []
							}
						]
					},
					{
						"objectId": "81C53EC7-07A9-4321-903E-37DBCBE07C45",
						"kind": "group",
						"name": "iconhalf",
						"originalName": "iconhalf",
						"maskFrame": null,
						"layerFrame": {
							"x": 126,
							"y": 168,
							"width": 52,
							"height": 36
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"children": [
							{
								"objectId": "D7C1F03B-C83C-4089-AE00-D60D97254EC9",
								"kind": "group",
								"name": "icon",
								"originalName": "icon",
								"maskFrame": null,
								"layerFrame": {
									"x": 126,
									"y": 168,
									"width": 36,
									"height": 36
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-icon-rdddmuyw.png",
									"frame": {
										"x": 126,
										"y": 168,
										"width": 36,
										"height": 36
									}
								},
								"children": []
							},
							{
								"objectId": "F8F10999-566C-4695-85CE-0A540BE35EB6",
								"kind": "text",
								"name": "caret",
								"originalName": "caret",
								"maskFrame": null,
								"layerFrame": {
									"x": 162,
									"y": 183,
									"width": 11,
									"height": 6
								},
								"visible": true,
								"metadata": {
									"opacity": 1,
									"string": "",
									"css": [
										"/*  copy: */",
										"font-family: FontAwesome;",
										"font-size: 18px;",
										"color: #FFFFFF;",
										"letter-spacing: 0px;"
									]
								},
								"image": {
									"path": "images/Layer-caret-rjhgmta5.png",
									"frame": {
										"x": 162,
										"y": 183,
										"width": 11,
										"height": 6
									}
								},
								"children": []
							},
							{
								"objectId": "29E7F5DB-0F18-440D-B025-3BD39622333D",
								"kind": "group",
								"name": "bg1",
								"originalName": "bg",
								"maskFrame": null,
								"layerFrame": {
									"x": 126,
									"y": 168,
									"width": 52,
									"height": 36
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-bg-mjlfn0y1.png",
									"frame": {
										"x": 126,
										"y": 168,
										"width": 52,
										"height": 36
									}
								},
								"children": []
							}
						]
					},
					{
						"objectId": "F7F36EE5-1A19-4B19-A4E2-D1486D260382",
						"kind": "group",
						"name": "Group_41",
						"originalName": "Group 4",
						"maskFrame": null,
						"layerFrame": {
							"x": 64,
							"y": 184,
							"width": 64,
							"height": 5
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Group_4-rjdgmzzf.png",
							"frame": {
								"x": 64,
								"y": 184,
								"width": 64,
								"height": 5
							}
						},
						"children": []
					},
					{
						"objectId": "40041BA2-67EC-49AB-A355-EB4E98CB1E8C",
						"kind": "group",
						"name": "icon_created1",
						"originalName": "icon-created",
						"maskFrame": null,
						"layerFrame": {
							"x": 78,
							"y": 168,
							"width": 36,
							"height": 36
						},
						"visible": false,
						"metadata": {
							"opacity": 1
						},
						"children": [
							{
								"objectId": "0531AF95-BD37-4B27-8CCA-AE9E76104FDF",
								"kind": "group",
								"name": "Group11",
								"originalName": "Group",
								"maskFrame": null,
								"layerFrame": {
									"x": 78,
									"y": 168,
									"width": 36,
									"height": 36
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-Group-mduzmufg.png",
									"frame": {
										"x": 78,
										"y": 168,
										"width": 36,
										"height": 36
									}
								},
								"children": []
							}
						]
					}
				]
			}
		]
	}
]
if (DeviceComponent) {DeviceComponent.Devices["iphone-6-silver"].deviceImageJP2 = false};
if (window.Framer) {window.Framer.Defaults.DeviceView = {"deviceScale":1,"selectedHand":"","deviceType":"fullscreen","contentScale":1,"orientation":0};
}
if (window.Framer) {window.Framer.Defaults.DeviceComponent = {"deviceScale":1,"selectedHand":"","deviceType":"fullscreen","contentScale":1,"orientation":0};
}
window.FramerStudioInfo = {"deviceImagesUrl":"\/_server\/resources\/DeviceImages","documentTitle":"minipipelinegraphhover.framer"};

Framer.Device = new Framer.DeviceView();
Framer.Device.setupContext();