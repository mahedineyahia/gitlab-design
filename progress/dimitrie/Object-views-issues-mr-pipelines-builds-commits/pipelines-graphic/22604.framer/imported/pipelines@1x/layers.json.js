window.__imported__ = window.__imported__ || {};
window.__imported__["pipelines@1x/layers.json.js"] = [
	{
		"objectId": "D881BC96-D28A-4801-AC30-C09DAD2CBBE3",
		"kind": "artboard",
		"name": "Artboard",
		"originalName": "Artboard",
		"maskFrame": null,
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 1003,
			"height": 611
		},
		"visible": true,
		"metadata": {},
		"backgroundColor": "rgba(255, 255, 255, 1)",
		"children": [
			{
				"objectId": "ED93530B-6D95-4946-B3C7-E5048752566A",
				"kind": "group",
				"name": "Graph",
				"originalName": "Graph",
				"maskFrame": null,
				"layerFrame": {
					"x": 30,
					"y": 33,
					"width": 931,
					"height": 469
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"children": [
					{
						"objectId": "14883760-C361-4F5D-A6BE-5D50C3F7AAE4",
						"kind": "group",
						"name": "DROPDOWN",
						"originalName": "DROPDOWN",
						"maskFrame": null,
						"layerFrame": {
							"x": 438,
							"y": 259,
							"width": 201,
							"height": 243
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-DROPDOWN-mtq4odm3.png",
							"frame": {
								"x": 438,
								"y": 259,
								"width": 201,
								"height": 243
							}
						},
						"children": [
							{
								"objectId": "47CDAB25-0431-4880-A493-D0643EAB97A5",
								"kind": "group",
								"name": "icon_running_copy",
								"originalName": "icon-running copy",
								"maskFrame": null,
								"layerFrame": {
									"x": 466,
									"y": 462,
									"width": 14,
									"height": 14
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"children": [
									{
										"objectId": "7C925C01-FFCC-49F1-B84F-955392855187",
										"kind": "group",
										"name": "Group_Copy",
										"originalName": "Group Copy",
										"maskFrame": null,
										"layerFrame": {
											"x": 466,
											"y": 462,
											"width": 14,
											"height": 14
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-Group_Copy-n0m5mjvd.png",
											"frame": {
												"x": 466,
												"y": 462,
												"width": 14,
												"height": 14
											}
										},
										"children": []
									},
									{
										"objectId": "F54BA669-19F2-4B77-8F88-3CDA3F4CCC5E",
										"kind": "group",
										"name": "Group",
										"originalName": "Group",
										"maskFrame": null,
										"layerFrame": {
											"x": 466,
											"y": 282,
											"width": 14,
											"height": 14
										},
										"visible": false,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-Group-rju0qke2.png",
											"frame": {
												"x": 466,
												"y": 282,
												"width": 14,
												"height": 14
											}
										},
										"children": []
									}
								]
							},
							{
								"objectId": "4E0A307D-35D1-46BF-AC00-973E00DC635C",
								"kind": "group",
								"name": "Group_2_Copy_13",
								"originalName": "Group 2 Copy 13",
								"maskFrame": null,
								"layerFrame": {
									"x": 488,
									"y": 462,
									"width": 77,
									"height": 15
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-Group_2_Copy_13-neuwqtmw.png",
									"frame": {
										"x": 488,
										"y": 462,
										"width": 77,
										"height": 15
									}
								},
								"children": [
									{
										"objectId": "B5798506-0973-458D-B09F-3911090BEDEC",
										"kind": "group",
										"name": "Group_9",
										"originalName": "Group 9",
										"maskFrame": null,
										"layerFrame": {
											"x": 488,
											"y": 462,
											"width": 77,
											"height": 15
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-Group_9-qju3otg1.png",
											"frame": {
												"x": 488,
												"y": 462,
												"width": 77,
												"height": 15
											}
										},
										"children": []
									}
								]
							},
							{
								"objectId": "2996DC8A-F74E-4EF5-8FC7-9E3CFF80A190",
								"kind": "group",
								"name": "Group_2_Copy_12",
								"originalName": "Group 2 Copy 12",
								"maskFrame": null,
								"layerFrame": {
									"x": 466,
									"y": 390,
									"width": 99,
									"height": 51
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-Group_2_Copy_12-mjk5nkrd.png",
									"frame": {
										"x": 466,
										"y": 390,
										"width": 99,
										"height": 51
									}
								},
								"children": [
									{
										"objectId": "C6AFF816-4667-44F9-851B-A276506A3A5D",
										"kind": "group",
										"name": "Group_91",
										"originalName": "Group 9",
										"maskFrame": null,
										"layerFrame": {
											"x": 466,
											"y": 390,
											"width": 99,
											"height": 51
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-Group_9-qzzbrky4.png",
											"frame": {
												"x": 466,
												"y": 390,
												"width": 99,
												"height": 51
											}
										},
										"children": []
									}
								]
							},
							{
								"objectId": "9FA05307-5C05-4B58-9F71-C454922F4D21",
								"kind": "group",
								"name": "Group_2_Copy_11",
								"originalName": "Group 2 Copy 11",
								"maskFrame": null,
								"layerFrame": {
									"x": 488,
									"y": 390,
									"width": 77,
									"height": 15
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-Group_2_Copy_11-ouzbmduz.png",
									"frame": {
										"x": 488,
										"y": 390,
										"width": 77,
										"height": 15
									}
								},
								"children": [
									{
										"objectId": "2F0EE9EE-FE26-4A8C-9E89-8292CD38A101",
										"kind": "group",
										"name": "Group_92",
										"originalName": "Group 9",
										"maskFrame": null,
										"layerFrame": {
											"x": 488,
											"y": 390,
											"width": 77,
											"height": 15
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-Group_9-mkywruu5.png",
											"frame": {
												"x": 488,
												"y": 390,
												"width": 77,
												"height": 15
											}
										},
										"children": []
									}
								]
							},
							{
								"objectId": "53CA2F00-AC8D-4FE0-B06A-A4DEA7E1958E",
								"kind": "group",
								"name": "Group_2_Copy_10",
								"originalName": "Group 2 Copy 10",
								"maskFrame": null,
								"layerFrame": {
									"x": 466,
									"y": 354,
									"width": 99,
									"height": 15
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-Group_2_Copy_10-ntndqtjg.png",
									"frame": {
										"x": 466,
										"y": 354,
										"width": 99,
										"height": 15
									}
								},
								"children": [
									{
										"objectId": "60FB25D8-36DB-47B4-BB19-E88AC24D1831",
										"kind": "group",
										"name": "Group_93",
										"originalName": "Group 9",
										"maskFrame": null,
										"layerFrame": {
											"x": 466,
											"y": 354,
											"width": 99,
											"height": 15
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-Group_9-njbgqji1.png",
											"frame": {
												"x": 466,
												"y": 354,
												"width": 99,
												"height": 15
											}
										},
										"children": []
									}
								]
							},
							{
								"objectId": "F544CA61-C8D3-479E-9885-0F6C753758EC",
								"kind": "group",
								"name": "Group_2_Copy_9",
								"originalName": "Group 2 Copy 9",
								"maskFrame": null,
								"layerFrame": {
									"x": 466,
									"y": 282,
									"width": 99,
									"height": 51
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-Group_2_Copy_9-rju0nenb.png",
									"frame": {
										"x": 466,
										"y": 282,
										"width": 99,
										"height": 51
									}
								},
								"children": [
									{
										"objectId": "13B6C7F0-871E-4E9F-BDE6-81CB27987F17",
										"kind": "group",
										"name": "Group_94",
										"originalName": "Group 9",
										"maskFrame": null,
										"layerFrame": {
											"x": 466,
											"y": 282,
											"width": 99,
											"height": 51
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-Group_9-mtncnkm3.png",
											"frame": {
												"x": 466,
												"y": 282,
												"width": 99,
												"height": 51
											}
										},
										"children": []
									}
								]
							},
							{
								"objectId": "E8F6303F-FB2A-431E-8575-A9999B7C4D76",
								"kind": "group",
								"name": "Group_2",
								"originalName": "Group 2",
								"maskFrame": null,
								"layerFrame": {
									"x": 488,
									"y": 271,
									"width": 137,
									"height": 36
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-Group_2-rthgnjmw.png",
									"frame": {
										"x": 488,
										"y": 271,
										"width": 137,
										"height": 36
									}
								},
								"children": [
									{
										"objectId": "8BCCD06C-B0DE-4FB7-8AC3-1E265B5307E6",
										"kind": "group",
										"name": "Group_95",
										"originalName": "Group 9",
										"maskFrame": null,
										"layerFrame": {
											"x": 488,
											"y": 282,
											"width": 77,
											"height": 15
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-Group_9-oejdq0qw.png",
											"frame": {
												"x": 488,
												"y": 282,
												"width": 77,
												"height": 15
											}
										},
										"children": []
									},
									{
										"objectId": "6E4AB1D5-386A-46E8-BE82-3F648FD5D85B",
										"kind": "group",
										"name": "Hover",
										"originalName": "Hover",
										"maskFrame": null,
										"layerFrame": {
											"x": 591,
											"y": 271,
											"width": 34,
											"height": 36
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-Hover-nku0quix.png",
											"frame": {
												"x": 591,
												"y": 271,
												"width": 34,
												"height": 36
											}
										},
										"children": []
									}
								]
							}
						]
					},
					{
						"objectId": "A864D151-BF30-4120-A97D-6D547C7638F3",
						"kind": "group",
						"name": "column4",
						"originalName": "column4",
						"maskFrame": null,
						"layerFrame": {
							"x": 775,
							"y": 33,
							"width": 186,
							"height": 77
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-column4-qtg2neqx.png",
							"frame": {
								"x": 775,
								"y": 33,
								"width": 186,
								"height": 77
							}
						},
						"children": [
							{
								"objectId": "5AD4A418-F40D-4D67-993C-DE8F45E3EF80",
								"kind": "group",
								"name": "Group_2_Copy_6",
								"originalName": "Group 2 Copy 6",
								"maskFrame": null,
								"layerFrame": {
									"x": 775,
									"y": 70,
									"width": 186,
									"height": 40
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-Group_2_Copy_6-nufenee0.png",
									"frame": {
										"x": 775,
										"y": 70,
										"width": 186,
										"height": 40
									}
								},
								"children": [
									{
										"objectId": "960A0F37-F22D-483B-8885-C3F68988508F",
										"kind": "group",
										"name": "Group_Copy_8",
										"originalName": "Group Copy 8",
										"maskFrame": null,
										"layerFrame": {
											"x": 786,
											"y": 80,
											"width": 20,
											"height": 20
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-Group_Copy_8-otywqtbg.png",
											"frame": {
												"x": 786,
												"y": 80,
												"width": 20,
												"height": 20
											}
										},
										"children": []
									},
									{
										"objectId": "E8D52131-00F9-40EE-A33E-35D2EDC7AD29",
										"kind": "text",
										"name": "Group_5",
										"originalName": "Group 5",
										"maskFrame": null,
										"layerFrame": {
											"x": 936,
											"y": 84,
											"width": 11,
											"height": 11
										},
										"visible": true,
										"metadata": {
											"opacity": 1,
											"string": "",
											"css": [
												"/* : */",
												"font-family: FontAwesome;",
												"font-size: 12px;",
												"color: #5C5C5C;",
												"letter-spacing: 0px;"
											]
										},
										"image": {
											"path": "images/Layer-Group_5-rthentix.png",
											"frame": {
												"x": 936,
												"y": 84,
												"width": 11,
												"height": 11
											}
										},
										"children": []
									},
									{
										"objectId": "C44B2F6B-C075-48A8-BFF3-1EA3F1D83904",
										"kind": "text",
										"name": "Group_3",
										"originalName": "Group 3",
										"maskFrame": null,
										"layerFrame": {
											"x": 813,
											"y": 84,
											"width": 75,
											"height": 15
										},
										"visible": true,
										"metadata": {
											"opacity": 1,
											"string": "review_stop",
											"css": [
												"/* review_stop: */",
												"font-family: SourceSansPro-Regular;",
												"font-size: 15px;",
												"color: #8C8C8C;",
												"letter-spacing: 0px;"
											]
										},
										"image": {
											"path": "images/Layer-Group_3-qzq0qjjg.png",
											"frame": {
												"x": 813,
												"y": 84,
												"width": 75,
												"height": 15
											}
										},
										"children": []
									},
									{
										"objectId": "8FADA0F1-7988-48E8-9110-65E2C462759E",
										"kind": "group",
										"name": "blockhover_copy",
										"originalName": "blockhover copy",
										"maskFrame": null,
										"layerFrame": {
											"x": 900,
											"y": 71,
											"width": 60,
											"height": 38
										},
										"visible": false,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-blockhover_copy-oezbreew.png",
											"frame": {
												"x": 900,
												"y": 71,
												"width": 60,
												"height": 38
											}
										},
										"children": []
									},
									{
										"objectId": "78BEE8C2-A296-497E-A217-B8C2C26A012B",
										"kind": "group",
										"name": "dothover_copy",
										"originalName": "dothover copy",
										"maskFrame": null,
										"layerFrame": {
											"x": 900,
											"y": 71,
											"width": 60,
											"height": 38
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-dothover_copy-nzhcruu4.png",
											"frame": {
												"x": 900,
												"y": 71,
												"width": 60,
												"height": 38
											}
										},
										"children": []
									}
								]
							}
						]
					},
					{
						"objectId": "E8BAB44D-B584-4C59-95C5-49A76D6B54BD",
						"kind": "group",
						"name": "column3",
						"originalName": "column3",
						"maskFrame": null,
						"layerFrame": {
							"x": 542,
							"y": 34,
							"width": 186,
							"height": 124
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-column3-rthcqui0.png",
							"frame": {
								"x": 542,
								"y": 34,
								"width": 186,
								"height": 124
							}
						},
						"children": [
							{
								"objectId": "5B58B4AD-B1C0-4D77-A42F-1EB979F7FEA2",
								"kind": "group",
								"name": "Group_2_Copy_61",
								"originalName": "Group 2 Copy 6",
								"maskFrame": null,
								"layerFrame": {
									"x": 542,
									"y": 118,
									"width": 186,
									"height": 40
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-Group_2_Copy_6-nui1oei0.png",
									"frame": {
										"x": 542,
										"y": 118,
										"width": 186,
										"height": 40
									}
								},
								"children": [
									{
										"objectId": "8A2C072A-EC41-4822-B74B-E5BA9E30F2F5",
										"kind": "group",
										"name": "Group_31",
										"originalName": "Group 3",
										"maskFrame": null,
										"layerFrame": {
											"x": 553,
											"y": 128,
											"width": 161,
											"height": 20
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-Group_3-oeeyqza3.png",
											"frame": {
												"x": 553,
												"y": 128,
												"width": 161,
												"height": 20
											}
										},
										"children": []
									},
									{
										"objectId": "7D3D3FF5-0FEB-448C-902B-2317DF49EEB5",
										"kind": "group",
										"name": "dothover_copy1",
										"originalName": "dothover copy",
										"maskFrame": null,
										"layerFrame": {
											"x": 667,
											"y": 119,
											"width": 60,
											"height": 38
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-dothover_copy-n0qzrdng.png",
											"frame": {
												"x": 667,
												"y": 119,
												"width": 60,
												"height": 38
											}
										},
										"children": []
									}
								]
							}
						]
					},
					{
						"objectId": "760365AC-F0BD-4D71-8CD9-35E0F9881D72",
						"kind": "group",
						"name": "column2",
						"originalName": "column2",
						"maskFrame": null,
						"layerFrame": {
							"x": 262,
							"y": 34,
							"width": 186,
							"height": 316
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-column2-nzywmzy1.png",
							"frame": {
								"x": 262,
								"y": 34,
								"width": 186,
								"height": 316
							}
						},
						"children": [
							{
								"objectId": "2E0EE0C0-24F5-4CC4-B104-DE4F06576805",
								"kind": "group",
								"name": "Group_6",
								"originalName": "Group 6",
								"maskFrame": null,
								"layerFrame": {
									"x": 262,
									"y": 70,
									"width": 186,
									"height": 280
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-Group_6-mkuwruuw.png",
									"frame": {
										"x": 262,
										"y": 70,
										"width": 186,
										"height": 280
									}
								},
								"children": [
									{
										"objectId": "1C362163-B4AF-4848-941B-62EEB8B51402",
										"kind": "group",
										"name": "Group_7",
										"originalName": "Group 7",
										"maskFrame": null,
										"layerFrame": {
											"x": 262,
											"y": 262,
											"width": 186,
											"height": 40
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-Group_7-mumznjix.png",
											"frame": {
												"x": 262,
												"y": 262,
												"width": 186,
												"height": 40
											}
										},
										"children": [
											{
												"objectId": "C7304236-1643-4B95-9423-1990E2709D8C",
												"kind": "group",
												"name": "Group_2_Copy_5",
												"originalName": "Group 2 Copy 5",
												"maskFrame": null,
												"layerFrame": {
													"x": 341,
													"y": 273,
													"width": 28,
													"height": 19
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"children": [
													{
														"objectId": "3BE29196-5BF8-4D3E-9539-5159F31C1E84",
														"kind": "group",
														"name": "Group_15",
														"originalName": "Group 15",
														"maskFrame": null,
														"layerFrame": {
															"x": 341,
															"y": 273,
															"width": 28,
															"height": 19
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-Group_15-m0jfmjkx.png",
															"frame": {
																"x": 341,
																"y": 273,
																"width": 28,
																"height": 19
															}
														},
														"children": [
															{
																"objectId": "28176B98-2C6B-4275-AAF2-AF0FFD5AE0EC",
																"kind": "text",
																"name": "Group_16",
																"originalName": "Group 16",
																"maskFrame": null,
																"layerFrame": {
																	"x": 349,
																	"y": 278,
																	"width": 12,
																	"height": 10
																},
																"visible": true,
																"metadata": {
																	"opacity": 1,
																	"string": "18",
																	"css": [
																		"/* 18: */",
																		"font-family: SourceSansPro-Regular;",
																		"font-size: 13px;",
																		"color: #8C8C8C;",
																		"letter-spacing: 0px;"
																	]
																},
																"image": {
																	"path": "images/Layer-Group_16-mjgxnzzc.png",
																	"frame": {
																		"x": 349,
																		"y": 278,
																		"width": 12,
																		"height": 10
																	}
																},
																"children": []
															}
														]
													}
												]
											},
											{
												"objectId": "B8BCDB95-5C66-4259-9CF5-0C50ED3995C4",
												"kind": "group",
												"name": "Group_Copy_7",
												"originalName": "Group Copy 7",
												"maskFrame": null,
												"layerFrame": {
													"x": 426,
													"y": 276,
													"width": 4,
													"height": 12
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-Group_Copy_7-qjhcq0rc.png",
													"frame": {
														"x": 426,
														"y": 276,
														"width": 4,
														"height": 12
													}
												},
												"children": [
													{
														"objectId": "5E81A7CD-0183-47D2-BA88-A6685D9DA12F",
														"kind": "group",
														"name": "Group_8",
														"originalName": "Group 8",
														"maskFrame": null,
														"layerFrame": {
															"x": 426,
															"y": 276,
															"width": 4,
															"height": 12
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-Group_8-nuu4mue3.png",
															"frame": {
																"x": 426,
																"y": 276,
																"width": 4,
																"height": 12
															}
														},
														"children": []
													}
												]
											},
											{
												"objectId": "86BDF2AD-E093-43B9-83FD-F1F1866B7031",
												"kind": "group",
												"name": "dothover",
												"originalName": "dothover",
												"maskFrame": null,
												"layerFrame": {
													"x": 387,
													"y": 263,
													"width": 60,
													"height": 38
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-dothover-odzcreyy.png",
													"frame": {
														"x": 387,
														"y": 263,
														"width": 60,
														"height": 38
													}
												},
												"children": []
											}
										]
									},
									{
										"objectId": "64A91F60-7CA2-4445-B317-9AE235D7EB40",
										"kind": "group",
										"name": "Group_2_Copy_62",
										"originalName": "Group 2 Copy 6",
										"maskFrame": null,
										"layerFrame": {
											"x": 262,
											"y": 214,
											"width": 186,
											"height": 40
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-Group_2_Copy_6-njrbotfg.png",
											"frame": {
												"x": 262,
												"y": 214,
												"width": 186,
												"height": 40
											}
										},
										"children": [
											{
												"objectId": "5216BAA4-1CCC-47CB-BA51-0B5985FB97E4",
												"kind": "group",
												"name": "Group_32",
												"originalName": "Group 3",
												"maskFrame": null,
												"layerFrame": {
													"x": 273,
													"y": 224,
													"width": 162,
													"height": 20
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-Group_3-ntixnkjb.png",
													"frame": {
														"x": 273,
														"y": 224,
														"width": 162,
														"height": 20
													}
												},
												"children": [
													{
														"objectId": "C91E6820-A815-4834-948A-AB65DBBFECAD",
														"kind": "group",
														"name": "Group_Copy_81",
														"originalName": "Group Copy 8",
														"maskFrame": null,
														"layerFrame": {
															"x": 273,
															"y": 224,
															"width": 20,
															"height": 20
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-Group_Copy_8-qzkxrty4.png",
															"frame": {
																"x": 273,
																"y": 224,
																"width": 20,
																"height": 20
															}
														},
														"children": []
													},
													{
														"objectId": "FFE7DDCC-2522-4721-8A11-405F81AFC03D",
														"kind": "group",
														"name": "Group_5_Copy",
														"originalName": "Group 5 Copy",
														"maskFrame": null,
														"layerFrame": {
															"x": 424,
															"y": 228,
															"width": 11,
															"height": 12
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-Group_5_Copy-rkzfn0re.png",
															"frame": {
																"x": 424,
																"y": 228,
																"width": 11,
																"height": 12
															}
														},
														"children": []
													}
												]
											},
											{
												"objectId": "059D5796-596D-4A8E-B544-024F3F33051D",
												"kind": "group",
												"name": "wholehover",
												"originalName": "wholehover",
												"maskFrame": null,
												"layerFrame": {
													"x": 262,
													"y": 214,
													"width": 186,
													"height": 40
												},
												"visible": false,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-wholehover-mdu5rdu3.png",
													"frame": {
														"x": 262,
														"y": 214,
														"width": 186,
														"height": 40
													}
												},
												"children": []
											},
											{
												"objectId": "F355232B-DA8E-4335-8EA6-82087D5C0A1D",
												"kind": "group",
												"name": "dothover_copy2",
												"originalName": "dothover copy",
												"maskFrame": null,
												"layerFrame": {
													"x": 387,
													"y": 215,
													"width": 60,
													"height": 38
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-dothover_copy-rjm1ntiz.png",
													"frame": {
														"x": 387,
														"y": 215,
														"width": 60,
														"height": 38
													}
												},
												"children": []
											}
										]
									},
									{
										"objectId": "2607263E-DFE3-4954-989B-4B74F571BA12",
										"kind": "group",
										"name": "Group_2_Copy_63",
										"originalName": "Group 2 Copy 6",
										"maskFrame": null,
										"layerFrame": {
											"x": 262,
											"y": 166,
											"width": 186,
											"height": 40
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-Group_2_Copy_6-mjywnzi2.png",
											"frame": {
												"x": 262,
												"y": 166,
												"width": 186,
												"height": 40
											}
										},
										"children": [
											{
												"objectId": "52CC0842-07A7-4466-947B-4DE380F431E4",
												"kind": "group",
												"name": "Group_33",
												"originalName": "Group 3",
												"maskFrame": null,
												"layerFrame": {
													"x": 273,
													"y": 176,
													"width": 161,
													"height": 20
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-Group_3-ntjdqza4.png",
													"frame": {
														"x": 273,
														"y": 176,
														"width": 161,
														"height": 20
													}
												},
												"children": []
											},
											{
												"objectId": "C9F259DA-211D-4168-9926-0DC46D6C1C15",
												"kind": "group",
												"name": "dothover_copy_2",
												"originalName": "dothover copy 2",
												"maskFrame": null,
												"layerFrame": {
													"x": 387,
													"y": 167,
													"width": 60,
													"height": 38
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-dothover_copy_2-qzlgmju5.png",
													"frame": {
														"x": 387,
														"y": 167,
														"width": 60,
														"height": 38
													}
												},
												"children": []
											}
										]
									},
									{
										"objectId": "3538734B-C8C9-45ED-8610-64F4E72CDDC9",
										"kind": "group",
										"name": "icon_running",
										"originalName": "icon-running",
										"maskFrame": null,
										"layerFrame": {
											"x": 273,
											"y": 128,
											"width": 20,
											"height": 20
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"children": [
											{
												"objectId": "5AAFDB8B-327D-4F92-90EA-B23DCE599927",
												"kind": "group",
												"name": "Group1",
												"originalName": "Group",
												"maskFrame": null,
												"layerFrame": {
													"x": 273,
													"y": 128,
													"width": 20,
													"height": 20
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-Group-nufbrkrc.png",
													"frame": {
														"x": 273,
														"y": 128,
														"width": 20,
														"height": 20
													}
												},
												"children": []
											}
										]
									},
									{
										"objectId": "0A3102ED-2AC7-4CAE-99E4-5A2DFC4CE9E4",
										"kind": "group",
										"name": "Group_2_Copy_64",
										"originalName": "Group 2 Copy 6",
										"maskFrame": null,
										"layerFrame": {
											"x": 262,
											"y": 118,
											"width": 186,
											"height": 40
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-Group_2_Copy_6-meezmtay.png",
											"frame": {
												"x": 262,
												"y": 118,
												"width": 186,
												"height": 40
											}
										},
										"children": [
											{
												"objectId": "D5F9F360-3E77-4A2B-B5DF-C7AD15035315",
												"kind": "group",
												"name": "Group_51",
												"originalName": "Group 5",
												"maskFrame": null,
												"layerFrame": {
													"x": 422,
													"y": 132,
													"width": 12,
													"height": 12
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-Group_5-rdvgouyz.png",
													"frame": {
														"x": 422,
														"y": 132,
														"width": 12,
														"height": 12
													}
												},
												"children": []
											},
											{
												"objectId": "40FA6A07-3E37-4D6F-85D3-3BC06E0B8FF0",
												"kind": "text",
												"name": "Group_34",
												"originalName": "Group 3",
												"maskFrame": null,
												"layerFrame": {
													"x": 300,
													"y": 132,
													"width": 52,
													"height": 15
												},
												"visible": true,
												"metadata": {
													"opacity": 1,
													"string": "rubocop",
													"css": [
														"/* rake db:migrate…: */",
														"font-family: SourceSansPro-Regular;",
														"font-size: 15px;",
														"color: #8C8C8C;",
														"letter-spacing: 0px;"
													]
												},
												"image": {
													"path": "images/Layer-Group_3-ndbgqtzb.png",
													"frame": {
														"x": 300,
														"y": 132,
														"width": 52,
														"height": 15
													}
												},
												"children": []
											},
											{
												"objectId": "3EBF3300-F2B1-4B2E-82D3-F2EC821FC604",
												"kind": "group",
												"name": "blockhover_copy1",
												"originalName": "blockhover copy",
												"maskFrame": null,
												"layerFrame": {
													"x": 387,
													"y": 119,
													"width": 60,
													"height": 38
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-blockhover_copy-m0vcrjmz.png",
													"frame": {
														"x": 387,
														"y": 119,
														"width": 60,
														"height": 38
													}
												},
												"children": []
											},
											{
												"objectId": "5A43606E-9859-4A2C-865B-FB0908FE0A96",
												"kind": "group",
												"name": "dothover_copy3",
												"originalName": "dothover copy",
												"maskFrame": null,
												"layerFrame": {
													"x": 387,
													"y": 119,
													"width": 60,
													"height": 38
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-dothover_copy-nue0mzyw.png",
													"frame": {
														"x": 387,
														"y": 119,
														"width": 60,
														"height": 38
													}
												},
												"children": []
											}
										]
									}
								]
							}
						]
					},
					{
						"objectId": "FE1F880D-2427-4070-BE81-5F946427FA62",
						"kind": "group",
						"name": "column1",
						"originalName": "column1",
						"maskFrame": null,
						"layerFrame": {
							"x": 30,
							"y": 34,
							"width": 186,
							"height": 76
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-column1-rkuxrjg4.png",
							"frame": {
								"x": 30,
								"y": 34,
								"width": 186,
								"height": 76
							}
						},
						"children": []
					},
					{
						"objectId": "75F1DC57-9148-49F7-856C-FFDA8C5F8D01",
						"kind": "group",
						"name": "Lines",
						"originalName": "Lines",
						"maskFrame": null,
						"layerFrame": {
							"x": 215,
							"y": 89,
							"width": 560,
							"height": 242
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"children": [
							{
								"objectId": "632C2D56-1F14-4696-8E07-691D45FB705D",
								"kind": "group",
								"name": "Lines_Copy_3",
								"originalName": "Lines Copy 3",
								"maskFrame": null,
								"layerFrame": {
									"x": 727,
									"y": 89,
									"width": 48,
									"height": 50
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-Lines_Copy_3-njmyqzje.png",
									"frame": {
										"x": 727,
										"y": 89,
										"width": 48,
										"height": 50
									}
								},
								"children": [
									{
										"objectId": "37604ECC-CA9C-4E03-96DC-6BFBB8B4E942",
										"kind": "group",
										"name": "Group_8_Copy_24",
										"originalName": "Group 8 Copy 24",
										"maskFrame": {
											"x": 0,
											"y": 0,
											"width": 25,
											"height": 25
										},
										"layerFrame": {
											"x": 727,
											"y": 114,
											"width": 25,
											"height": 25
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-Group_8_Copy_24-mzc2mdrf.png",
											"frame": {
												"x": 727,
												"y": 114,
												"width": 25,
												"height": 25
											}
										},
										"children": []
									},
									{
										"objectId": "5C41EDBF-395A-4584-A709-43DF95F9112D",
										"kind": "group",
										"name": "Group_8_Copy_22",
										"originalName": "Group 8 Copy 22",
										"maskFrame": {
											"x": 0,
											"y": 0,
											"width": 25,
											"height": 25
										},
										"layerFrame": {
											"x": 750,
											"y": 89,
											"width": 25,
											"height": 25
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-Group_8_Copy_22-num0muve.png",
											"frame": {
												"x": 750,
												"y": 89,
												"width": 25,
												"height": 25
											}
										},
										"children": []
									}
								]
							},
							{
								"objectId": "489C18CC-8E30-4474-92C0-73DD146919E9",
								"kind": "group",
								"name": "Lines_Copy_2",
								"originalName": "Lines Copy 2",
								"maskFrame": null,
								"layerFrame": {
									"x": 495,
									"y": 89,
									"width": 48,
									"height": 50
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-Lines_Copy_2-ndg5qze4.png",
									"frame": {
										"x": 495,
										"y": 89,
										"width": 48,
										"height": 50
									}
								},
								"children": [
									{
										"objectId": "99B401C2-AFAB-464F-B1A9-F429C01A22F2",
										"kind": "group",
										"name": "Group_8_Copy_241",
										"originalName": "Group 8 Copy 24",
										"maskFrame": {
											"x": 0,
											"y": 0,
											"width": 25,
											"height": 25
										},
										"layerFrame": {
											"x": 518,
											"y": 114,
											"width": 25,
											"height": 25
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-Group_8_Copy_24-otlcndax.png",
											"frame": {
												"x": 518,
												"y": 114,
												"width": 25,
												"height": 25
											}
										},
										"children": []
									},
									{
										"objectId": "3916A3E2-ED47-481C-9B02-6856A2CE3302",
										"kind": "group",
										"name": "Group_8_Copy_221",
										"originalName": "Group 8 Copy 22",
										"maskFrame": {
											"x": 0,
											"y": 0,
											"width": 25,
											"height": 25
										},
										"layerFrame": {
											"x": 495,
											"y": 89,
											"width": 25,
											"height": 25
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-Group_8_Copy_22-mzkxnkez.png",
											"frame": {
												"x": 495,
												"y": 89,
												"width": 25,
												"height": 25
											}
										},
										"children": []
									}
								]
							},
							{
								"objectId": "AB0545C8-ECCE-4311-B75F-F0E3586A2481",
								"kind": "group",
								"name": "Lines_Copy",
								"originalName": "Lines Copy",
								"maskFrame": null,
								"layerFrame": {
									"x": 447,
									"y": 89,
									"width": 48,
									"height": 242
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-Lines_Copy-quiwntq1.png",
									"frame": {
										"x": 447,
										"y": 89,
										"width": 48,
										"height": 242
									}
								},
								"children": [
									{
										"objectId": "D43874EB-B635-44D7-BA67-373FDCC3624F",
										"kind": "group",
										"name": "Group_8_Copy_28",
										"originalName": "Group 8 Copy 28",
										"maskFrame": {
											"x": 0,
											"y": 0,
											"width": 25,
											"height": 25
										},
										"layerFrame": {
											"x": 447,
											"y": 306,
											"width": 25,
											"height": 25
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-Group_8_Copy_28-rdqzodc0.png",
											"frame": {
												"x": 447,
												"y": 306,
												"width": 25,
												"height": 25
											}
										},
										"children": []
									},
									{
										"objectId": "E4503EF6-B528-4799-BE4A-67AD0D9826EC",
										"kind": "group",
										"name": "Group_8_Copy_27",
										"originalName": "Group 8 Copy 27",
										"maskFrame": {
											"x": 0,
											"y": 0,
											"width": 25,
											"height": 25
										},
										"layerFrame": {
											"x": 447,
											"y": 258,
											"width": 25,
											"height": 25
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-Group_8_Copy_27-rtq1mdnf.png",
											"frame": {
												"x": 447,
												"y": 258,
												"width": 25,
												"height": 25
											}
										},
										"children": []
									},
									{
										"objectId": "43A43767-9F76-451A-8031-755D25FECAE9",
										"kind": "group",
										"name": "Group_8_Copy_26",
										"originalName": "Group 8 Copy 26",
										"maskFrame": {
											"x": 0,
											"y": 0,
											"width": 25,
											"height": 25
										},
										"layerFrame": {
											"x": 447,
											"y": 210,
											"width": 25,
											"height": 25
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-Group_8_Copy_26-ndnbndm3.png",
											"frame": {
												"x": 447,
												"y": 210,
												"width": 25,
												"height": 25
											}
										},
										"children": []
									},
									{
										"objectId": "F423646B-58E7-4E00-BF88-F473D1A81583",
										"kind": "group",
										"name": "Group_8_Copy_25",
										"originalName": "Group 8 Copy 25",
										"maskFrame": {
											"x": 0,
											"y": 0,
											"width": 25,
											"height": 25
										},
										"layerFrame": {
											"x": 447,
											"y": 162,
											"width": 25,
											"height": 25
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-Group_8_Copy_25-rjqymzy0.png",
											"frame": {
												"x": 447,
												"y": 162,
												"width": 25,
												"height": 25
											}
										},
										"children": []
									},
									{
										"objectId": "EC3EA34C-A497-4868-95ED-B9469F18BC20",
										"kind": "group",
										"name": "Group_8_Copy_242",
										"originalName": "Group 8 Copy 24",
										"maskFrame": {
											"x": 0,
											"y": 0,
											"width": 25,
											"height": 25
										},
										"layerFrame": {
											"x": 447,
											"y": 114,
											"width": 25,
											"height": 25
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-Group_8_Copy_24-rumzruez.png",
											"frame": {
												"x": 447,
												"y": 114,
												"width": 25,
												"height": 25
											}
										},
										"children": []
									},
									{
										"objectId": "3CC8BC7A-CDF3-4F85-AB8F-97D94F54DBFF",
										"kind": "group",
										"name": "Group_8_Copy_222",
										"originalName": "Group 8 Copy 22",
										"maskFrame": {
											"x": 0,
											"y": 0,
											"width": 25,
											"height": 25
										},
										"layerFrame": {
											"x": 470,
											"y": 89,
											"width": 25,
											"height": 25
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-Group_8_Copy_22-m0ndoejd.png",
											"frame": {
												"x": 470,
												"y": 89,
												"width": 25,
												"height": 25
											}
										},
										"children": []
									}
								]
							},
							{
								"objectId": "C4D70395-65F7-4FB1-9E35-DF1B0DE87055",
								"kind": "group",
								"name": "Lines1",
								"originalName": "Lines",
								"maskFrame": null,
								"layerFrame": {
									"x": 215,
									"y": 89,
									"width": 48,
									"height": 242
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-Lines-qzrenzaz.png",
									"frame": {
										"x": 215,
										"y": 89,
										"width": 48,
										"height": 242
									}
								},
								"children": [
									{
										"objectId": "76B7EFB6-4667-4448-B1C1-B2F9F2566936",
										"kind": "group",
										"name": "Group_8_Copy_281",
										"originalName": "Group 8 Copy 28",
										"maskFrame": {
											"x": 0,
											"y": 0,
											"width": 25,
											"height": 25
										},
										"layerFrame": {
											"x": 238,
											"y": 306,
											"width": 25,
											"height": 25
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-Group_8_Copy_28-nzzcn0vg.png",
											"frame": {
												"x": 238,
												"y": 306,
												"width": 25,
												"height": 25
											}
										},
										"children": []
									},
									{
										"objectId": "5853BAB5-C43B-4C44-BDB2-0C76DD609804",
										"kind": "group",
										"name": "Group_8_Copy_271",
										"originalName": "Group 8 Copy 27",
										"maskFrame": {
											"x": 0,
											"y": 0,
											"width": 25,
											"height": 25
										},
										"layerFrame": {
											"x": 238,
											"y": 258,
											"width": 25,
											"height": 25
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-Group_8_Copy_27-ntg1m0jb.png",
											"frame": {
												"x": 238,
												"y": 258,
												"width": 25,
												"height": 25
											}
										},
										"children": []
									},
									{
										"objectId": "23CAD5B4-83BD-4C06-9404-E651CE90DD22",
										"kind": "group",
										"name": "Group_8_Copy_261",
										"originalName": "Group 8 Copy 26",
										"maskFrame": {
											"x": 0,
											"y": 0,
											"width": 25,
											"height": 25
										},
										"layerFrame": {
											"x": 238,
											"y": 210,
											"width": 25,
											"height": 25
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-Group_8_Copy_26-mjndquq1.png",
											"frame": {
												"x": 238,
												"y": 210,
												"width": 25,
												"height": 25
											}
										},
										"children": []
									},
									{
										"objectId": "3520C71A-7DCE-48FE-88C1-7D775EA206A1",
										"kind": "group",
										"name": "Group_8_Copy_251",
										"originalName": "Group 8 Copy 25",
										"maskFrame": {
											"x": 0,
											"y": 0,
											"width": 25,
											"height": 25
										},
										"layerFrame": {
											"x": 238,
											"y": 162,
											"width": 25,
											"height": 25
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-Group_8_Copy_25-mzuymem3.png",
											"frame": {
												"x": 238,
												"y": 162,
												"width": 25,
												"height": 25
											}
										},
										"children": []
									},
									{
										"objectId": "D80350B5-7ABA-4CE3-AA47-1AAB1A678BEC",
										"kind": "group",
										"name": "Group_8_Copy_243",
										"originalName": "Group 8 Copy 24",
										"maskFrame": {
											"x": 0,
											"y": 0,
											"width": 25,
											"height": 25
										},
										"layerFrame": {
											"x": 238,
											"y": 114,
											"width": 25,
											"height": 25
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-Group_8_Copy_24-rdgwmzuw.png",
											"frame": {
												"x": 238,
												"y": 114,
												"width": 25,
												"height": 25
											}
										},
										"children": []
									},
									{
										"objectId": "AC1BEC5E-F614-4911-9284-D44B321D8918",
										"kind": "group",
										"name": "Group_8_Copy_223",
										"originalName": "Group 8 Copy 22",
										"maskFrame": {
											"x": 0,
											"y": 0,
											"width": 25,
											"height": 25
										},
										"layerFrame": {
											"x": 215,
											"y": 89,
											"width": 25,
											"height": 25
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-Group_8_Copy_22-qumxqkvd.png",
											"frame": {
												"x": 215,
												"y": 89,
												"width": 25,
												"height": 25
											}
										},
										"children": []
									}
								]
							}
						]
					}
				]
			}
		]
	}
]